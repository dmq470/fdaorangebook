﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace OrangeBookScraper
{
    class Program
    {
        private static CookieCollection _cookies  = new CookieCollection();

        private static HtmlWeb _webGetChild;

        static void Main(string[] args)
        {
            var webGet = new HtmlWeb {UseCookies = true, PostResponse = AfterRequest};
                //, PreRequest = BeforeRequest, PostResponse = AfterRequest };

            _webGetChild = new HtmlWeb {UseCookies = true, PreRequest = BeforeRequest};

            var validTags = new Dictionary<string, HtmlNode>();

            const string hrefPattern = "(?<=^|>)[^><]+?(?=<|$)";

            var hrefRegex = new Regex( hrefPattern );

            ProcessLetters( webGet, hrefRegex, validTags );
        }

        private static bool BeforeRequest( HttpWebRequest request )
        {
            request.CookieContainer = new CookieContainer();

            foreach ( Cookie cookie in _cookies )
            {
                request.CookieContainer.Add( cookie );
            }
            
            return true;
        }

        private static void AfterRequest( HttpWebRequest request, HttpWebResponse response )
        {
            _cookies = request.CookieContainer.GetCookies( request.RequestUri );
        }

        const string BaseFdaUri = @"http://www.accessdata.fda.gov/scripts/cder/drugsatfda/";
                
        private static void ProcessLetters( HtmlWeb webGet, Regex hrefRegex, Dictionary<string, HtmlNode> validTags )
        {
            Dictionary<string, HtmlNode> tags;

            var baseUrl = new Uri( BaseFdaUri );

            for ( var i = 65; i < 91; i++ )
            {
                var startingLetter = Char.ConvertFromUtf32( i );

                Trace.WriteLine( "Processing Letter: " + startingLetter );

                var url = BaseFdaUri + "/index.cfm?fuseaction=Search.SearchResults_Browse&DrugInitial=" + startingLetter;

                var document = webGet.Load( url );
                
                var pages = RetrieveAdditionalPages( document );

                tags = RetrieveDrugLinks( hrefRegex, validTags, document );

                if ( pages == null ) continue;

                foreach ( var node in pages )
                {
                    // Visit the node and get the links there
                    
                    
                    var href = node.Value.Attributes["href"].Value.ToString( CultureInfo.InvariantCulture );

                    var finalUrl = baseUrl + href;

                    Trace.WriteLine( "URL: " + finalUrl );

                    //webGet.PreRequest += request =>
                    //{
                    //    request.CookieContainer = new CookieContainer();
                    //    return true;
                    //};

                    var pageDoc = _webGetChild.Load(finalUrl, "GET");
                    
                    
                   tags = RetrieveDrugLinks( hrefRegex, validTags, pageDoc );

                    

                }
                // Need to go to each drug link then determine if we need to go to the next level or not

                foreach ( var htmlNode in tags )
                {
                    var hrefUrl = htmlNode.Value.Attributes["href"].Value;
                    
                    var finalUrl = new Uri( baseUrl, hrefUrl );

                    var drugDocument = _webGetChild.Load( finalUrl.ToString(), "GET" );

                    var innerDrugLinks = drugDocument.DocumentNode.SelectNodes( "//td//a[@href]" );

                    var innerTags = new Dictionary<string, string>();

                    var node = htmlNode;

                    foreach ( var innerDrugLink in innerDrugLinks.Where( innerDrugLink => innerDrugLink.InnerHtml == node.Key ) )
                    {
                        innerTags.Add( innerDrugLink.InnerText, innerDrugLink.Attributes["href"].Value  );
                        
                        Trace.WriteLine( "InnerDrugLink for " + htmlNode.Key + ": " + innerDrugLink.InnerHtml  );
                    }

                    // If there are inner links, need to redirect to detail page

                    // Once we are at the detail page find something that can transform an html table to a DataTable
                }

                Trace.WriteLine( "Testing" );
            }
        }

        private const string currentDrugLink = @"index.cfm?fuseaction=Search.Set_Current_Drug";

        private static Dictionary<string, HtmlNode> RetrieveDrugLinks( Regex hrefRegex, Dictionary<string, HtmlNode> validTags, HtmlDocument document )
        {
            var tags = document.DocumentNode.SelectNodes( "//td//ul//li//a[@href]" );

            foreach ( var tag in tags )
            {
                //const string drugNameLink = "index.cfm?fuseaction=Search.Overview&DrugName=";

                //if ( !tag.InnerHtml.Contains( drugNameLink ) ) continue;

                //var matches = hrefRegex.Matches( tag.InnerHtml );

                //var matchValue = matches[1].Groups[0].Value;

                if (!validTags.Keys.Contains(tag.InnerHtml))
                {
                    validTags.Add( tag.InnerHtml, tag );
                }

                Trace.WriteLine( tag.InnerHtml );
            }

            return validTags;
        }

        private static Dictionary<string, HtmlNode> RetrieveAdditionalPages( HtmlDocument document )
        {
            var pages = document.DocumentNode.SelectNodes( "//a[contains(@href,'StepSize')]" );

            var distinctPages = new Dictionary<string, HtmlNode>();

            if (pages != null )
            {
                foreach ( var page in pages.Where( page => ! distinctPages.ContainsKey( page.InnerText ) ) )
                {
                    distinctPages.Add( page.InnerText, page );
                }
            }
            
            return distinctPages;
        }
    }
}
