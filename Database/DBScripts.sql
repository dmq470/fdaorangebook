--bulk insert dbo.Product from 'C:\Projects\FDAOrangeBook\drugsatfda\Product.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 1)

bulk insert dbo.AppDoc from 'C:\Projects\FDAOrangeBook\drugsatfda\AppDoc.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.AppDocType_Lookup from 'C:\Projects\FDAOrangeBook\drugsatfda\AppDocType_Lookup.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.application from 'C:\Projects\FDAOrangeBook\drugsatfda\application.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.ChemType_Lookup from 'C:\Projects\FDAOrangeBook\drugsatfda\ChemTypeLookup.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.DocType_lookup from 'C:\Projects\FDAOrangeBook\drugsatfda\DocType_lookup.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.Product from 'C:\Projects\FDAOrangeBook\drugsatfda\Product.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.Product_tecode from 'C:\Projects\FDAOrangeBook\drugsatfda\Product_tecode.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.RegActionDate from 'C:\Projects\FDAOrangeBook\drugsatfda\RegActionDate.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)
bulk insert dbo.ReviewClass_Lookup from 'C:\Projects\FDAOrangeBook\drugsatfda\ReviewClass_Lookup.txt' with  (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n', FIRSTROW = 2)

delete from AppDoc
delete from ChemType_Lookup
delete from DocType_lookup
delete from Product
delete from Product_tecode
delete from RegActionDate
delete from ReviewClass_Lookup
delete from [Application]
delete from AppDocType_Lookup


select * from Product